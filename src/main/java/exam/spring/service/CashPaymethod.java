/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exam.spring.service;

import org.springframework.stereotype.Service;

/**
 *
 * @author steven
 */
@Service
@Cash
public class CashPaymethod implements Paymethod{

    @Override
    public void pay(String account, double amount) {
        System.out.println(" cash version" + " account " + account + " amount " + amount);
    }
    
}
