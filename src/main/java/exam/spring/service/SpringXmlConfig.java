/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exam.spring.service;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author steven
 */
public class SpringXmlConfig {
    public ApplicationContext Config(){
        return new ClassPathXmlApplicationContext("/application.xml");
    }
}
