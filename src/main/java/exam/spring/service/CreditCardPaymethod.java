/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exam.spring.service;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

/**
 *
 * @author steven
 */
@Service
@CreditCard
@Scope(scopeName = BeanDefinition.SCOPE_PROTOTYPE)
public class CreditCardPaymethod implements Paymethod{

    @Override
    public void pay(String account, double amount) {
        System.out.println(" creditCard version" + " account " + account + " amount " + amount);
    }
    
}
