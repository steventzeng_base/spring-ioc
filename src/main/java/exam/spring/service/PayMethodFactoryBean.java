/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exam.spring.service;

import org.springframework.beans.factory.FactoryBean;


/**
 *
 * @author steven
 */
public class PayMethodFactoryBean implements FactoryBean<Paymethod>{

    @Override
    public Paymethod getObject() throws Exception {
        return new CreditCardPaymethod();
    }

    @Override
    public Class<?> getObjectType() {
        return CreditCardPaymethod.class;
    }

    @Override
    public boolean isSingleton() {
        return false;
    }
    
}
