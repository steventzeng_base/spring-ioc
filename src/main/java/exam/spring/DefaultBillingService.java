/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exam.spring;

import exam.spring.service.Cash;
import exam.spring.service.CreditCard;
import exam.spring.service.Paymethod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;


/**
 *
 * @author steven
 */

@Service
@Qualifier("defaultBillingService")
public class DefaultBillingService implements BillingService {

    @CreditCard
    @Autowired
    private  Paymethod creditPaymethod;
    
    @Cash
    @Autowired
    private  Paymethod cashPaymethod;

    public DefaultBillingService() {
    }

    @Override
    public void pay(User user, ShoppingCar car) {
        cashPaymethod.pay(user.getAccount(), car.getTotal());
    }
}
