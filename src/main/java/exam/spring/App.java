/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exam.spring;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 *
 * @author steven
 */
public class App {
    public static void main(String[] args) {
        User user = new User("userA");
        ShoppingCar car = new ShoppingCar(1000);
//        ApplicationContext  context = new SpringXmlConfig().Config();
        ApplicationContext context = new AnnotationConfigApplicationContext(SpringConfig.class);
        BillingService billingService = context.getBean("defaultBillingService", BillingService.class);
        billingService.pay(user, car);
        BillingService service2 = context.getBean("service2", BillingService.class);
        service2.pay(user, car);
    }
}
