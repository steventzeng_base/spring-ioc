/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exam.spring;

/**
 *
 * @author steven
 */
public class ShoppingCar {

    private double total;

    public ShoppingCar() {
    }

    public ShoppingCar(double total) {
        this.total = total;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

}
