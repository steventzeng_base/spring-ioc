/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exam.spring;

import exam.spring.service.CashPaymethod;
import exam.spring.service.Paymethod;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 *
 * @author steven
 */
@Configuration
@ComponentScan(basePackages = "exam.spring")
public class SpringConfig {
//    @Bean
//    Paymethod paymethod(){
//        return new CashPaymethod();
//    }
//    
//    @Bean
//    BillingService billingService(){
//        return new DefaultBillingService(paymethod());
//    }
}
