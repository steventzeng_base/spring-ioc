/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exam.spring;

import exam.spring.service.CreditCard;
import exam.spring.service.Paymethod;
import javax.inject.Provider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author steven
 */
@Service("service2")
public class BillingService2 implements BillingService {

    @Autowired
    @CreditCard
    Provider<Paymethod> paymethod;

    @Override
    public void pay(User user, ShoppingCar car) {
        paymethod.get().pay(user.getAccount(), car.getTotal());

    }

}
